// import {useState} from 'react';
import {Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}){
	// console.log(props);
		// result: php-laravel (courseData[0])
// every component receives information in a form of object
	// console.log(typeof props);

	// object destructuring
	const {name, description, price, _id} = courseProp

	// react hooks - useState -> store its state
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue);
	// const [count, setCount] = useState(0);
	// console.log(useState(0));
	// const [seatcount, setSeatCount] = useState(10);
	// const [enrolleescount, setEnrolleeCount] = useState(0);

	// // function enroll(){
	// // 	setCount(count + 1);
	// // 	console.log(`Enrollees: ${count}`);
	// // };
	// function enroll(){
	// 	// setSeatCount(seatcount - 1);
	// 	// setEnrolleeCount(enrolleescount + 1);
	// 	if(seatcount === 0){
	// 		alert("No more seats available, Please check again later.")
	// 	} else {
	// 		setSeatCount(seatcount - 1);
	// 		setEnrolleeCount(enrolleescount + 1);
	// 	}

	// }

	return (

		<Row>

			<Col xs={12} md={12}>

				<Card className="cardCourseCard p-3">

					<Card.Body>

						<Card.Title className="mb-3"><h2>{name}</h2></Card.Title>

						<Card.Subtitle>Course Description:</Card.Subtitle>

						<Card.Text>{description}</Card.Text>

						<Card.Subtitle>Course Price:</Card.Subtitle>

						<Card.Text>{price}</Card.Text>
{/*
						<Card.Text>Enrollees: {enrolleescount}</Card.Text>

						<Card.Text>Seats: {seatcount}</Card.Text>*/}

{/*						<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
						
						<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>

					</Card.Body>

				</Card>

			</Col>

		</Row>

	)
};