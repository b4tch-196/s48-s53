import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	// Allows us to consume the User context object and it's properties to use user validation
	// useContext(Provider)
	const {user, setUser} = useContext(UserContext);
	console.log(user);
	
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	function loginUser(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		// Syntax:
			// fetch('url', options)
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 196!"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})
			}
		});

		// Set the email of the authenticated user in the local storage
			// Syntax:
				// localStorage.setItem('proertyName/key', value);
		// localStorage.setItem("email", email);

		// Access user information through localStorage to the update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// When states change components are re-rendered and the AppNavbar component will be updated based on the credentials.

		// setUser({
		// 	email: localStorage.getItem('email')
		// });

		// Clear input fields after submission
		setEmail('');
		setPassword('');
	};

	const retrieveUserDetails = (token) => {

		// Default option method for fetch is 'GET'
		fetch('http://localhost:4000/users/getUserDetails',{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};

	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password]);


	return (

		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Login</h1>
		<Form onSubmit={e => loginUser(e)}> 


			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter you email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>


			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<p>Not yet registered? <Link to="/register">Register Here</Link></p>

		{ 
			isActive ? //if

			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Login
			</Button>

			: //else

			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Login
			</Button>
		}


		</Form>
		</>

	)
}