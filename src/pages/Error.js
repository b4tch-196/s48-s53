import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Error(){

	return (

		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>The page you're looking for cannot be found.</p>
				<Button variant="primary" as={Link} to="/">Go Back</Button>
			</Col>
		</Row>
	)

}
