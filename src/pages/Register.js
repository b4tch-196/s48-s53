import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);
	const history = useNavigate();
	// state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// const [password2, setPassword2] = useState('');

	// state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// check if values are successfully binded
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e){
		// prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);
			//result: boolean

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "info",
					text: "The email that you're trying to register already exist"
				});
			} else {
				fetch('http://localhost:4000/users', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data.email){
						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Thank you for registering"
						});

						history("/login");

					} else {
						Swal.fire({
							title: "Registration failed",
							icon: "error",
							text: "Something went wrong, try again"
						});
					};
				});
			};
		});

		// clear input fields
		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');

	}

	// Syntax:
		// useEffect(() => {}, [])

	// useEffect(() => {
	// 	// validation to enable the submit button when all fields are populated and both passwords match
	// 	if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	// 		setIsActive(true);
	// 	} else {
	// 		setIsActive(false);
	// 	}

	// }, [email, password1, password2]);

		useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if((email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, firstName, lastName, mobileNo, password]);

	return (

		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Register Here</h1>
		<Form onSubmit={e => registerUser(e)}> 


			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your first name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your last name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your 11-digit mobile number"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>


			<Form.Group controlId="userEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter you email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>


			<Form.Group controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Set your password"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>


			{/*<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Input your password again"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>
			</Form.Group>*/}


		{ 
			isActive ? //if

			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Register
			</Button>

			: //else

			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Register
			</Button>
		}


		</Form>
		</>
	)
}

